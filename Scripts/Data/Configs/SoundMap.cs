using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[CreateAssetMenu(fileName = "SoundMap", menuName = "Data/SoundMap")]
public class SoundMap : ScriptableObject, IDefinitionSingle
{
    [SerializeField] private AudioClip[] _soundData;

    private AudioMixerGroup _musicAudioMixerGroup;
    private AudioMixerGroup _soundAudioMixerGroup;
    private Dictionary<string, AudioClip> _soundMap;

    public AudioMixerGroup MusicAudioMixerGroup => _musicAudioMixerGroup;
    public AudioMixerGroup SoundAudioMixerGroup => _soundAudioMixerGroup;

    private void GenerateMap()
    {
        _soundMap = new Dictionary<string, AudioClip>();
        foreach (var sound in _soundData)
        {
            _soundMap.Add(sound.name, sound);
        }
    }

    public AudioClip GetAudio(string audioName)
    {
        if (_soundMap == null)
        {
            GenerateMap();
        }

        return _soundMap[audioName];
    }
}