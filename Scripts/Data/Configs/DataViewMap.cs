using System;
using UnityEngine;

[Serializable] public class CurrencyViewDictionary : SerializableDictionary<CurrencyType, ViewData> { }
[Serializable] public class HeroCharacteristicsViewDictionary : SerializableDictionary<HeroParameterType, ViewData> { }

[CreateAssetMenu(fileName = "DataViewMap", menuName = "Data/DataViewMap")]
public class DataViewMap : ScriptableObject, IDefinitionSingle
{
    [SerializeField] private CurrencyView _currencyView;
    [SerializeField] private HeroCharacteristicsView _heroCharacteristicsView;

    public string GetCurrencyView(CurrencyType currencyType, float value) => _currencyView.GetView(currencyType, value);
    public string GetHeroCharacteristicsViewView(HeroParameterType heroParameterType, float value) => _heroCharacteristicsView.GetView(heroParameterType, value);
}

[Serializable]
public class ViewData
{
    [SerializeField] private string _format;
    [SerializeField] private int _valueMultiplier;

    public string Format => _format;
    public int ValueMultiplier => _valueMultiplier;
}

public abstract class DataView<M, P> where M : SerializableDictionary<P, ViewData>
{
    [SerializeField] private ViewData _defaultView;
    [SerializeField] private M _dataViewMap;

    public string GetView(P dataType, float value)
    {
        if (!_dataViewMap.TryGetValue(dataType, out var viewData))
        {
            viewData = _defaultView;
        }

        return (value * viewData.ValueMultiplier).ToString(viewData.Format);
    }
}

[Serializable] public class CurrencyView : DataView<CurrencyViewDictionary, CurrencyType> { }

[Serializable] public class HeroCharacteristicsView : DataView<HeroCharacteristicsViewDictionary, HeroParameterType> { }