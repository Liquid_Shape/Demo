using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable] public class HeroParametersProgressionDictionary : SerializableDictionary<HeroParameterType, HeroCharacteristicData> { }

public enum HeroParameterType
{
    Damage = 0,
    AttackSpeed = 1,
    CriticalChance = 6,
    CriticalDamage = 7,

    Health = 2,
    HealthRegeneration = 3,

    ExpPerKill = 4,
    ExpPerLevel = 5
}

[CreateAssetMenu(fileName = "HeroCharacteristicsProgression", menuName = "Data/HeroCharacteristicsProgression")]
public class HeroCharacteristicsProgression : ScriptableObject, IDefinitionSingle
{    
    [SerializeField] private float _projectileSpeed;
    [SerializeField] private float _ranage;
    [SerializeField] private float _rotationSpeed;
    [SerializeField] private float _startExp;
    [SerializeField] private HeroParametersProgressionDictionary _progressionMap;

    public HeroCharacteristicProgressionData GetParameterCharacteristics(HeroParameterType parameter, int level)
    {
        if (!_progressionMap.TryGetValue(parameter, out var heroParameterData))
        {
            throw new Exception($"HeroParameter data for \"{parameter}\" is not found!");
        }

        if (!HasLevel(parameter, level))
        {
            throw new Exception($"Level \"{level}\" data for \"{parameter}\" is not found!");
        }

        return heroParameterData.ProgressData[level];
    }

    public float ProjectileSpeed => _projectileSpeed;
    public float Ranage => _ranage;
    public float RotationSpeed => _rotationSpeed;
    public float StartExp => _startExp;

    public bool HasLevel(HeroParameterType parameter, int level)
    {
        return level >= 0 && level < _progressionMap[parameter].ProgressData.Count;
    }
}

[Serializable]
public class HeroCharacteristicData
{
    [SerializeField] private HeroCharacteristicProgressionData[] _progressData;

    public IReadOnlyList<HeroCharacteristicProgressionData> ProgressData => _progressData;
}

[Serializable]
public class HeroCharacteristicProgressionData
{
    [SerializeField] private float _value;
    [SerializeField] private float _cost;

    public float Value => _value;
    public float Cost => _cost;
}