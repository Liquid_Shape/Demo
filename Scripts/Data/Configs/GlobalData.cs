using System;
using UnityEngine;

[CreateAssetMenu(fileName = "GlobalData", menuName = "Data/GlobalData")]
public class GlobalData : ScriptableObject, IDefinitionSingle
{
    [SerializeField] private float _overDamageMultiplier;
    [SerializeField] private CameraData _cameraData;
    [SerializeField] private AudioData _audioData;
    
    public float OverDamageMultiplier => _overDamageMultiplier;
    public CameraData CameraData => _cameraData;    
    public AudioData AudioData => _audioData;    
}

[Serializable]
public class CameraData
{
    [SerializeField] private float _smallShakeValue;
    [SerializeField] private float _smallShakeTime;
    [SerializeField] private float _middleShakeValue;
    [SerializeField] private float _middleShakeTime;
    [SerializeField] private float _bigShakeValue;
    [SerializeField] private float _bigShakeTime;

    public float SmallShakeValue => _smallShakeValue;
    public float SmallShakeTime => _smallShakeTime;
    public float MiddleShakeValue => _middleShakeValue;
    public float MiddleShakeTime => _middleShakeTime;
    public float BigShakeValue => _bigShakeValue;
    public float BigShakeTime => _bigShakeTime;
}

[Serializable]
public class AudioData
{
    [SerializeField] private string _ambientName;
    [SerializeField] private string _bossSpawnSoundName;
    [SerializeField] private string _uiClickSoundName;
    [SerializeField] private string _upgradeCoreParameterSoundName;

    public string AmbientName => _ambientName;
    public string BossSpawnSoundName => _bossSpawnSoundName;
    public string UIClickSoundName => _uiClickSoundName;
    public string UpgradeCoreParameterSoundName => _upgradeCoreParameterSoundName; 
}