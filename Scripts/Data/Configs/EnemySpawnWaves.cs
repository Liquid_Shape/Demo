using System;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemySpawnWaves", menuName = "Data/EnemySpawnWaves")]
public class EnemySpawnWaves : ScriptableObject, IDefinitionSingle
{
    [SerializeField] private SpawnWaveData[] _spawnWaveData;

    public SpawnWaveData[] SpawnWaveData => _spawnWaveData;
}

[Serializable]
public class SpawnWaveData
{
    [SerializeField] private float _dealay;
    [SerializeField] private EnemySpawnData[] _enemySpawnData;

    public float Delay => _dealay;
    public EnemySpawnData[] EnemySpawnData => _enemySpawnData;
}

[Serializable]
public class EnemySpawnData
{
    [SerializeField] private EnemyID _enemyID;
    [SerializeField] private EnemyType _enemyType;
    [SerializeField] private float _delay;

    public EnemyID EnemyID => _enemyID;
    public EnemyType EnemyType => _enemyType;
    public float Delay => _delay;
}