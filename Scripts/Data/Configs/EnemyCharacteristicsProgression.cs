using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable] public class EnemyDataDictionary : SerializableDictionary<EnemyID, EnemyData> { }

public enum EnemyID 
{
    Casual = 0,
    Strong = 1,
    Faster = 2,
    Boss = 3
}

public enum EnemyType
{
    Regular, Boss
}

[CreateAssetMenu(fileName = "EnemyCharacteristicsProgression", menuName = "Data/EnemyCharacteristicsProgression")]
public class EnemyCharacteristicsProgression : ScriptableObject, IDefinitionSingle
{
    [SerializeField] private EnemyDataDictionary _regularEnemyData;
    [SerializeField] private EnemyDataDictionary _bossEnemyData;

    private Dictionary<EnemyType, EnemyDataDictionary> _progressionMap;

    private void GenerateMap()
    {
        _progressionMap = new Dictionary<EnemyType, EnemyDataDictionary>()
        {
            {EnemyType.Regular, _regularEnemyData},
            {EnemyType.Boss, _bossEnemyData}
        };
    }

    public EnemyCharacteristics GetEnemyCharacteristics(EnemyType enemyType, EnemyID enemyID, int waveNumber)
    {
        if (_progressionMap == null)
        {
            GenerateMap();
        }

        if (!_progressionMap[enemyType].TryGetValue(enemyID, out var enemyData))
        {
            throw new Exception($"Enemy data for \"{enemyID}\" is not found!");
        }

        waveNumber = Mathf.Clamp(waveNumber, 0, enemyData.ProgressData.Count - 1);

        return new EnemyCharacteristics(
            enemyData.PrefabName,
            enemyType,
            enemyID,
            enemyData.StopDistance,
            enemyData.MoveSpeed,
            enemyData.AttackSpeed,
            enemyData.ProgressData[waveNumber].Health,
            enemyData.ProgressData[waveNumber].AttackDamage);        
    }
}

[Serializable]
public class EnemyData
{
    [SerializeField] private string _prefabName;
    [SerializeField] private float _stopDistance;
    [SerializeField] private float _moveSpeed;
    [SerializeField] private float _attackSpeed;
    [SerializeField] private EnemyProgressData[] _progressData;

    public string PrefabName => _prefabName;
    public float StopDistance => _stopDistance;
    public float MoveSpeed => _moveSpeed;
    public float AttackSpeed => _attackSpeed;
    public IReadOnlyList<EnemyProgressData> ProgressData => _progressData;
}

[Serializable]
public class EnemyProgressData
{
    [SerializeField] private float _health;
    [SerializeField] private float _attackDamage;

    public float Health => _health;
    public float AttackDamage => _attackDamage;
}

public class EnemyCharacteristics
{
    public readonly string PrefabName;
    public readonly EnemyType EnemyType;
    public readonly EnemyID EnemyID;
    public readonly float StopDistance;
    public readonly float MoveSpeed;
    public readonly float AttackSpeed;
    public readonly float Health;
    public readonly float AttackDamage;

    public EnemyCharacteristics(
        string prefabName,
        EnemyType enemyType,
        EnemyID enemyID,
        float stopDistance,
        float moveSpeed,
        float attackSpeed,
        float health,
        float attackDamage)
    {
        PrefabName = prefabName;
        EnemyType = enemyType;
        EnemyID = enemyID;
        StopDistance = stopDistance;
        MoveSpeed = moveSpeed;
        AttackSpeed = attackSpeed;
        Health = health;
        AttackDamage = attackDamage;
    }
}