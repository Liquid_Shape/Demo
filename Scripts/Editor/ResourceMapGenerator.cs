using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEditor;

public static class ResourceMapGenerator
{
    public static readonly string ResourceMapPath = $"Assets/Resources/{ResourceLoader.ResourcesMapName}.txt";

    [MenuItem("Utils/Resources/Generate Resource Map")]
    public static void GenerateResourceDirectoryMap()
    {
        var dir = Path.GetDirectoryName(ResourceMapPath);
        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }

        File.WriteAllText(ResourceMapPath, JsonConvert.SerializeObject(GetMap(), Formatting.Indented));
        AssetDatabase.ImportAsset(ResourceMapPath, ImportAssetOptions.ForceUpdate);
    }

    private static Dictionary<string, string> GetMap()
    {
        var allPaths = AssetDatabase.GetAllAssetPaths();
        var result = new Dictionary<string, string>();
        foreach (var path in allPaths)
        {
            if (path.Contains($"{Path.AltDirectorySeparatorChar}Editor{Path.AltDirectorySeparatorChar}") ||
                !path.Contains($"{Path.AltDirectorySeparatorChar}Resources{Path.AltDirectorySeparatorChar}") ||
                Directory.Exists(path))
            {
                continue;
            }

            var name = Path.GetFileNameWithoutExtension(path);
            if (result.TryGetValue(name, out var assetPath))
            {
                throw new ArgumentException($"Asset with name {name}, has different variants in resources folders: " +
                                            $"{string.Join(",", new[] {assetPath, path})}");
            }

            result.Add(name, path.Split($"Resources{Path.AltDirectorySeparatorChar}")[1].Split('.')[0]);
        }

        return result;
    }
}