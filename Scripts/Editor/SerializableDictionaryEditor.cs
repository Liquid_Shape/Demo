using UnityEditor;

[CustomPropertyDrawer(typeof(EnemyDataDictionary))]
public class EnemyDataDictionaryPropertyDrawer : SerializableDictionaryPropertyDrawer { }

[CustomPropertyDrawer(typeof(HeroParametersProgressionDictionary))]
public class HeroParametersProgressionDictionaryDrawer : SerializableDictionaryPropertyDrawer { }

[CustomPropertyDrawer(typeof(CurrencyViewDictionary))]
public class CurrencyViewDictionaryDrawer : SerializableDictionaryPropertyDrawer { }

[CustomPropertyDrawer(typeof(HeroCharacteristicsViewDictionary))]
public class HeroCharacteristicsViewDictionaryDrawer : SerializableDictionaryPropertyDrawer { }