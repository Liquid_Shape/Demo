using DG.Tweening;
using MessagePipe;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VContainer;

public class TimeSpeedPanel : MonoBehaviour
{
    [SerializeField] private Button _speedDownButton;
    [SerializeField] private Button _speedUpButton;
    [SerializeField] private TextMeshProUGUI _timeSpeedValueText;
    [Space(10)]
    [SerializeField] private float[] _timeSpeedUpPoints;

    private IPublisher<UIButtonClickEvent> _uiButtonClickEventIPublisher;

    private List<float> _timeSpeedPoints;
    private int _speedValueCounter;

    [Inject]
    public void Initialize(IPublisher<UIButtonClickEvent> uiButtonClickEventIPublisher)
    {
        _uiButtonClickEventIPublisher = uiButtonClickEventIPublisher;
    }

    private void Awake()
    {
        _speedDownButton.onClick.AddListener(() => 
        {
            SpeedDown();
            _uiButtonClickEventIPublisher.Publish(new UIButtonClickEvent());            
        });
        _speedUpButton.onClick.AddListener(() =>
        {
            SpeedUp();
            _uiButtonClickEventIPublisher.Publish(new UIButtonClickEvent());
        });
        _timeSpeedPoints = new List<float>() { 0f };
        _timeSpeedPoints.AddRange(_timeSpeedUpPoints);
        _speedValueCounter = 1;
        SetTimeSpeed(_timeSpeedPoints[_speedValueCounter]);
    }

    private void SpeedDown()
    {
        _speedValueCounter = Mathf.Clamp(--_speedValueCounter, 0, _timeSpeedPoints.Count - 1);
        SetTimeSpeed(_timeSpeedPoints[_speedValueCounter]);
        UpdateButton();
    }

    private void SpeedUp()
    {  
        _speedValueCounter = Mathf.Clamp(++_speedValueCounter, 0, _timeSpeedPoints.Count - 1);
        SetTimeSpeed(_timeSpeedPoints[_speedValueCounter]);
        UpdateButton();
    }

    private void SetTimeSpeed(float speed)
    {
        Time.timeScale = speed;
        _timeSpeedValueText.text = string.Concat("x", speed.ToString("F1").Replace(',', '.'));
        _timeSpeedValueText.rectTransform.DOKill(true);
        _timeSpeedValueText.rectTransform.DOBubbleAnimation();
    }

    private void UpdateButton()
    {
        _speedDownButton.interactable = _speedValueCounter > 0;
        _speedUpButton.interactable = _speedValueCounter < _timeSpeedPoints.Count - 1;
    }
}