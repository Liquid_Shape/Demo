using DG.Tweening;
using MessagePipe;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VContainer;

public class UpgradeSlot : MonoBehaviour, IDisposable,
    IMessageHandler<HeroParameterUpdateEvent>,
    IMessageHandler<CurrencyChangedEvent>
{
    [SerializeField] private HeroParameterType _parameterType;
    [Space(10)]
    [SerializeField] private Button _upgradeButton;
    [SerializeField] private TextMeshProUGUI _currentValueText;
    [SerializeField] private TextMeshProUGUI _nextValueText;
    [SerializeField] private TextMeshProUGUI _costText;
    
    private IPublisher<UIButtonClickEvent> _uiButtonClickEventPublisher;
    private IDisposable _heroParameterUpdateEventSubribe;
    private IDisposable _currencyChangedEventSubribe;

    private HeroModule _heroModule;
    private CurrencyModule _currencyModule;
    private DataViewMap _dataViewMap;
    
    private HeroCharacteristicProgressionData _parameter;
    private HeroCharacteristicProgressionData _nextParameter;

    public HeroParameterType ParameterType => _parameterType;

    [Inject]
    public void Initialize(HeroModule heroModule, CurrencyModule currencyModule, DefinitionsProvider definitionsProvider,
        IPublisher<UIButtonClickEvent> uiButtonClickEventPublisher,
        ISubscriber<HeroParameterUpdateEvent> heroParameterUpdateEventSubriber,
        ISubscriber<CurrencyChangedEvent> currencyChangedEventSubriber)
    {
        _heroModule = heroModule;
        _currencyModule = currencyModule;
        _dataViewMap = definitionsProvider.Get<DataViewMap>();
        _uiButtonClickEventPublisher = uiButtonClickEventPublisher;
        _heroParameterUpdateEventSubribe = heroParameterUpdateEventSubriber.Subscribe(this);
        _currencyChangedEventSubribe = currencyChangedEventSubriber.Subscribe(this);
    }

    private void Awake()
    {
        _upgradeButton.onClick.AddListener(() =>
        {
            _heroModule.TryUpgradeParameter(_parameterType);
            _uiButtonClickEventPublisher.Publish(new UIButtonClickEvent());
        });
    }

    public void UpdateParameterInfo(HeroCharacteristicProgressionData newParameter, HeroCharacteristicProgressionData nextParameter)
    {
        _parameter = newParameter;
        _nextParameter = nextParameter;

        if (_nextParameter == null)
        {
            gameObject.SetActive(false);
            return;
        }

        UpdateParameterText(_currentValueText, _dataViewMap.GetHeroCharacteristicsViewView(_parameterType, _parameter.Value));
        UpdateParameterText(_nextValueText, _dataViewMap.GetHeroCharacteristicsViewView(_parameterType, _nextParameter.Value));
        UpdateParameterText(_costText, _dataViewMap.GetCurrencyView(CurrencyType.Exp, _nextParameter.Cost));
        UpdateButtonActiveState(_currencyModule.GetCurrency(CurrencyType.Exp));
    }

    private void UpdateParameterText(TextMeshProUGUI text, string value)
    {
        text.text = value;
        text.rectTransform.DOKill(true);
        text.rectTransform.DOBubbleAnimation();
    }

    private void UpdateButtonActiveState(float curentExpValue)
    {
        _upgradeButton.interactable = _nextParameter != null && _nextParameter.Cost <= curentExpValue;
    }

    public void Handle(HeroParameterUpdateEvent message)
    {
        if (message.Parameter != _parameterType)
        {
            return;
        }

        UpdateParameterInfo(message.NewParameter, message.NextParameter);
    }

    public void Dispose()
    {
        _heroParameterUpdateEventSubribe.Dispose();
        _currencyChangedEventSubribe.Dispose();
    }

    public void Handle(CurrencyChangedEvent message)
    {
        if (message.CurrencyType != CurrencyType.Exp)
        {
            return;
        }

        UpdateButtonActiveState(message.NewValue);
    }
}