using MessagePipe;
using System;
using UnityEngine;
using UnityEngine.UI;
using VContainer;

public class UpgradeSlotsPageButton : MonoBehaviour, IDisposable,
    IMessageHandler<CurrencyChangedEvent>
{
    [SerializeField] private Button _button;
    [SerializeField] private Image _canUpgradeImage;
    [SerializeField] private LayoutElement _activelayoutElement;
    [SerializeField] private LayoutElement _deactivelayoutElement;
    [Space(10)]
    [SerializeField] private UpgradeSlot[] _upgradeSlots;

    private HeroModule _heroModule;
    private IDisposable _currencyChangedEventSubribe;
    
    public Button Button => _button;

    [Inject]
    public void Initialize(IObjectResolver objectResolver, HeroModule heroModule,
        ISubscriber<CurrencyChangedEvent> currencyChangedEventSubriber)
    {
        _heroModule = heroModule;
        _currencyChangedEventSubribe = currencyChangedEventSubriber.Subscribe(this);

        foreach (var slot in _upgradeSlots)
        {
            objectResolver.Inject(slot);
        }
    }

    public void SetActiveState(bool state)
    {
        _button.interactable = state;
        _activelayoutElement.enabled = state;
        _deactivelayoutElement.enabled = !state;
    }

    public void Handle(CurrencyChangedEvent message)
    {
        if (message.CurrencyType != CurrencyType.Exp)
        {
            return;
        }

        var canUpgrade = false;
        foreach (var slot in _upgradeSlots)
        {
            canUpgrade |= _heroModule.CanUpgradeParameter(slot.ParameterType);
        }

        _canUpgradeImage.gameObject.SetActive(canUpgrade);
    }

    public void Dispose()
    {
        _currencyChangedEventSubribe.Dispose();
        foreach (var slot in _upgradeSlots)
        {
            slot.Dispose();
        }
    }
}