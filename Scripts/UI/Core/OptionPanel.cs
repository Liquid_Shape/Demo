using MessagePipe;
using UnityEngine;
using UnityEngine.UI;
using VContainer;

public class OptionPanel : MonoBehaviour
{
    [SerializeField] private Image _mainMenuButtonIcon;
    [SerializeField] private Image _alternativeMenuButtonIcon;
    [SerializeField] private Button _menuButton;
    [SerializeField] private Button _audioButton;
    [SerializeField] private Button _vibroButton;
    [SerializeField] private Button _exitButton;
    [SerializeField] private GameObject _panel;

    IPublisher<UIButtonClickEvent> _uiButtonClickEventPublisher;

    [Inject]
    public void Initialize(IPublisher<UIButtonClickEvent> uiButtonClickEventPublisher)
    {
        _uiButtonClickEventPublisher = uiButtonClickEventPublisher;
    }

    private void Awake()
    {
        _menuButton.onClick.AddListener(() =>
        {
            _panel.SetActive(!_panel.activeSelf);
            _mainMenuButtonIcon.gameObject.SetActive(!_panel.activeSelf);
            _alternativeMenuButtonIcon.gameObject.SetActive(_panel.activeSelf);

            _uiButtonClickEventPublisher.Publish(new UIButtonClickEvent());
        });
        _audioButton.onClick.AddListener(() => 
        {
            _uiButtonClickEventPublisher.Publish(new UIButtonClickEvent());
        });
        _vibroButton.onClick.AddListener(() =>
        {
            _uiButtonClickEventPublisher.Publish(new UIButtonClickEvent());
        });
        _exitButton.onClick.AddListener(() =>
        {
            _uiButtonClickEventPublisher.Publish(new UIButtonClickEvent());
        });
    }
}