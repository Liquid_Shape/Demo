using DG.Tweening;
using MessagePipe;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VContainer;

public class UpgradeWindow : MonoBehaviour, IDisposable,
    IMessageHandler<HeroChangeHealthEvent>,
    IMessageHandler<CurrencyChangedEvent>
{
    [SerializeField] private Image _healthBarImage;
    [SerializeField] private TextMeshProUGUI _healthBarText;
    [Space(10)]
    [SerializeField] private TextMeshProUGUI _expText;
    [SerializeField] private TextMeshProUGUI _goldText;
    [Space(10)]
    [SerializeField] private UpgradeSlotsPageButton _attackUpgradePanelButton;
    [SerializeField] private UpgradeSlotsPageButton _defenceUpgradePanelButton;
    [SerializeField] private UpgradeSlotsPageButton _resourceUpgradePanelButton;
    [SerializeField] private GameObject _attckUpgradePanel;
    [SerializeField] private GameObject _defenceUpgradePanel;
    [SerializeField] private GameObject _resourceUpgradePanel;

    private IPublisher<UIButtonClickEvent> _uiButtonClickEventIPublisher;
    private IDisposable _heroChangeHealthEventSubribe;
    private IDisposable _currencyChangedEventSubribe;

    private DataViewMap _dataViewMap;
    private Dictionary<CurrencyType, TextMeshProUGUI> _currensyMap;

    private int _lasHealthValue;
    private int _lasMaxHealthValue;

    private void Awake()
    {
        _attackUpgradePanelButton.Button.onClick.AddListener(() =>
        {
            HideAllUpgradePanels();
            _attckUpgradePanel.SetActive(true);
            _attackUpgradePanelButton.SetActiveState(false);
            _defenceUpgradePanelButton.SetActiveState(true);
            _resourceUpgradePanelButton.SetActiveState(true);
            _uiButtonClickEventIPublisher.Publish(new UIButtonClickEvent());
        });
        _defenceUpgradePanelButton.Button.onClick.AddListener(() =>
        {
            HideAllUpgradePanels();
            _defenceUpgradePanel.SetActive(true);
            _attackUpgradePanelButton.SetActiveState(true);
            _defenceUpgradePanelButton.SetActiveState(false);
            _resourceUpgradePanelButton.SetActiveState(true);
            _uiButtonClickEventIPublisher.Publish(new UIButtonClickEvent());
        });
        _resourceUpgradePanelButton.Button.onClick.AddListener(() =>
        {
            HideAllUpgradePanels();
            _resourceUpgradePanel.SetActive(true);
            _attackUpgradePanelButton.SetActiveState(true);
            _defenceUpgradePanelButton.SetActiveState(true);
            _resourceUpgradePanelButton.SetActiveState(false);
            _uiButtonClickEventIPublisher.Publish(new UIButtonClickEvent());
        });        
    }

    private void Start()
    {
        _attackUpgradePanelButton.Button.onClick.Invoke();
    }

    [Inject]
    public void Initialize(IObjectResolver objectResolver, DefinitionsProvider definitionsProvider,
        IPublisher<UIButtonClickEvent> uiButtonClickEventIPublisher,
        ISubscriber<HeroChangeHealthEvent> heroChangeHealthEventSubriber,
        ISubscriber<CurrencyChangedEvent> currencyChangedEventSubriber)
    {
        _dataViewMap = definitionsProvider.Get<DataViewMap>();
        _uiButtonClickEventIPublisher = uiButtonClickEventIPublisher;
        _heroChangeHealthEventSubribe = heroChangeHealthEventSubriber.Subscribe(this);
        _currencyChangedEventSubribe = currencyChangedEventSubriber.Subscribe(this);
        _currensyMap = new Dictionary<CurrencyType, TextMeshProUGUI>()
        {
            {CurrencyType.Exp, _expText},
            {CurrencyType.Gold, _goldText}
        };        
        objectResolver.Inject(_attackUpgradePanelButton);
        objectResolver.Inject(_defenceUpgradePanelButton);
        objectResolver.Inject(_resourceUpgradePanelButton);
    }

    private void HideAllUpgradePanels()
    {
        _attckUpgradePanel.SetActive(false);
        _defenceUpgradePanel.SetActive(false);
        _resourceUpgradePanel.SetActive(false);
    }

    public void Handle(HeroChangeHealthEvent message)
    {
        _healthBarImage.fillAmount = message.CurentHealth / message.MaxHealth;

        var health = (int)message.CurentHealth;
        var maxHealth = (int)message.MaxHealth;
        if (_lasHealthValue == health && _lasMaxHealthValue == maxHealth)
        {
            return;
        }

        _healthBarText.text = string.Concat(health.ToString(), "/", maxHealth.ToString());
        _healthBarText.rectTransform.DOKill(true);
        _healthBarText.rectTransform.DOBubbleAnimation();        
        _lasHealthValue = health;
        _lasMaxHealthValue = maxHealth;
    }

    public void Handle(CurrencyChangedEvent message)
    {
        UpdateCurencyCountView(message.CurrencyType, message.NewValue);
    }

    public void Dispose()
    {
        _heroChangeHealthEventSubribe?.Dispose();
        _currencyChangedEventSubribe?.Dispose();
        _attackUpgradePanelButton?.Dispose();
        _defenceUpgradePanelButton?.Dispose();
        _resourceUpgradePanelButton?.Dispose();
    }

    private void UpdateCurencyCountView(CurrencyType currencyType, float currencyValue)
    {
        _currensyMap[currencyType].text = _dataViewMap.GetCurrencyView(currencyType, currencyValue);
        _currensyMap[currencyType].rectTransform.DOKill(true);
        _currensyMap[currencyType].rectTransform.DOBubbleAnimation();
    }
}