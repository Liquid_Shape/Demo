using MessagePipe;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using VContainer;

public class GameOverWindow : MonoBehaviour
{
    [SerializeField] private Button _restartButton;

    private IPublisher<UIButtonClickEvent> _uiButtonClickEventIPublisher;

    [Inject]
    public void Initialize(IPublisher<UIButtonClickEvent> uiButtonClickEventIPublisher)
    {
        _uiButtonClickEventIPublisher = uiButtonClickEventIPublisher;
    }

    private void Awake()
    {
        _restartButton.onClick.AddListener(() =>
        {            
            Restart();
            _uiButtonClickEventIPublisher.Publish(new UIButtonClickEvent());
        });
    }

    private void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}