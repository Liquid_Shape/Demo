using MessagePipe;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VContainer;

public class WaveCounterPanel : MonoBehaviour, IDisposable,
    IMessageHandler<SpawnWaveStartEvent>,
    IMessageHandler<EnemyDieEvent>
{
    [SerializeField] private TextMeshProUGUI _counterText;
    [SerializeField] private Image _progressImage;

    private IDisposable _spawnWaveStartEventSubscribe;
    private IDisposable _enemyDieEventSubscribe;

    private int _waveEnemyCount;
    private int _waveEnemyLast;

    [Inject]
    public void Initialize(ISubscriber<SpawnWaveStartEvent> spawnWaveStartEventSubscriber,
        ISubscriber<EnemyDieEvent> enemyDieEventSubscriber)
    {
        _spawnWaveStartEventSubscribe = spawnWaveStartEventSubscriber.Subscribe(this);
        _enemyDieEventSubscribe = enemyDieEventSubscriber.Subscribe(this);
        _progressImage.fillAmount = 0f;
    }

    public void Dispose()
    {
        _spawnWaveStartEventSubscribe.Dispose();
        _enemyDieEventSubscribe.Dispose();
    }

    public void Handle(SpawnWaveStartEvent message)
    {
        _counterText.text = (message.WaveNumber + 1).ToString();
        _progressImage.fillAmount = 0;
        _waveEnemyLast = _waveEnemyCount = message.WaveEnemyCount;
    }

    public void Handle(EnemyDieEvent message)
    {
        _waveEnemyLast--;
        _progressImage.fillAmount = 1f - _waveEnemyLast / (float)_waveEnemyCount;
    }
}