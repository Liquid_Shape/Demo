using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    [Range(0f, 3f)] [SerializeField] private float _gizmosSphereRadius;
    [SerializeField] private Color _gizmosSphereColor;

    void OnDrawGizmos()
    {
        Gizmos.color = _gizmosSphereColor;
        Gizmos.DrawWireSphere(transform.position, _gizmosSphereRadius);
    }
}