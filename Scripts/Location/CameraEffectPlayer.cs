using UnityEngine;
using Cinemachine;
using Cysharp.Threading.Tasks;
using System;

public class CameraEffectPlayer : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera _virtualCamera;

    private CinemachineBasicMultiChannelPerlin _basicMultiChannelPerlin;

    private void Awake()
    {
        _basicMultiChannelPerlin = _virtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
    }

    public void Shake(float shakeValue, float time)
    {
        ShakeTask().Forget();

        async UniTask ShakeTask()
        {
            _basicMultiChannelPerlin.m_AmplitudeGain = shakeValue;
            _basicMultiChannelPerlin.m_FrequencyGain = shakeValue;

            await UniTask.Delay(TimeSpan.FromSeconds(time));

            _basicMultiChannelPerlin.m_AmplitudeGain = 0f;
            _basicMultiChannelPerlin.m_FrequencyGain = 0f;
        }
    }
}
