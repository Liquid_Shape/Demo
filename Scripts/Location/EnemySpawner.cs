using Cysharp.Threading.Tasks;
using MessagePipe;
using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using VContainer;
using VContainer.Unity;

public struct SpawnWaveStartEvent
{
    public readonly int WaveNumber;
    public readonly int WaveEnemyCount;

    public SpawnWaveStartEvent(int waweNumber, int waveEnemyCount)
    {
        WaveNumber = waweNumber;
        WaveEnemyCount = waveEnemyCount;
    }
}

public struct EnemySpawnEvent
{
    public readonly Enemy Enemy;

    public EnemySpawnEvent(Enemy enemy)
    {
        Enemy = enemy;
    }
}

public class EnemySpawner : MonoBehaviour, IMessageHandler<HeroDieEvent>, IDisposable
{
    private const int _lastSpawnPointCount = 3;

    private IObjectResolver _objectResolver;
    private EnemySpawnWaves _spawnData;
    private EnemyCharacteristicsProgression _regularEnemyParametersProgression;
    private ResourceLoader _resourceLoader;
    private AnalyticsModule _analyticsModule;
    private UnitModule _unitModule;

    private SpawnPoint[] _spawnPoints;
    private Queue<SpawnPoint> _lastSpawnPoint = new Queue<SpawnPoint>();

    private IPublisher<EnemySpawnEvent> _enemySpawnEventPublisher;
    private IPublisher<SpawnWaveStartEvent> _spawnWaveStartEventPublisher;
    private IDisposable _heroDieEventSubscribe;

    private CancellationTokenSource _spawnCancellationTokenSource = new CancellationTokenSource();

    [Inject]
    public void Initialize(IObjectResolver objectResolver, DefinitionsProvider definitionsProvider, ResourceLoader resourceLoader, AnalyticsModule analyticsModule, UnitModule unitModule,
        IPublisher<EnemySpawnEvent> enemySpawnEventPublisher,
        IPublisher<SpawnWaveStartEvent> spawnWaveStartEventPublisher,
        ISubscriber<HeroDieEvent> heroDieEventSubscriber)
    {
        _objectResolver = objectResolver;
        _analyticsModule = analyticsModule;
        _unitModule = unitModule;
        _spawnData = definitionsProvider.Get<EnemySpawnWaves>();
        _regularEnemyParametersProgression = definitionsProvider.Get<EnemyCharacteristicsProgression>();
        _resourceLoader = resourceLoader;
        _spawnPoints = GetComponentsInChildren<SpawnPoint>();

        _enemySpawnEventPublisher = enemySpawnEventPublisher;
        _spawnWaveStartEventPublisher = spawnWaveStartEventPublisher;

        _heroDieEventSubscribe = heroDieEventSubscriber.Subscribe(this);
        _spawnCancellationTokenSource.RegisterRaiseCancelOnDestroy(this);
    }

    private void Start()
    {
        SpawnTask().Forget();
    }

    private async UniTaskVoid SpawnTask()
    {
        for (int i = 0; i < _spawnData.SpawnWaveData.Length; i++)
        {
            await SpawnWaveTask(_spawnData.SpawnWaveData[i], i)
                .AttachExternalCancellation(_spawnCancellationTokenSource.Token);
        }

        var waveNumber = _spawnData.SpawnWaveData.Length;
        while (true)
        {
            await SpawnWaveTask(_spawnData.SpawnWaveData[_spawnData.SpawnWaveData.Length - 1], waveNumber++)
                .AttachExternalCancellation(_spawnCancellationTokenSource.Token);
        }
    }

    private async UniTask SpawnWaveTask(SpawnWaveData waveData, int waveNumber)
    {
        await UniTask.Delay(TimeSpan.FromSeconds(waveData.Delay))
            .AttachExternalCancellation(_spawnCancellationTokenSource.Token);

        _spawnWaveStartEventPublisher.Publish(new SpawnWaveStartEvent(waveNumber, waveData.EnemySpawnData.Length));

        for (int i = 0; i < waveData.EnemySpawnData.Length; i++)
        {
            await SpawnWithDelay(
                waveData.EnemySpawnData[i].EnemyType,
                waveData.EnemySpawnData[i].EnemyID,
                waveData.EnemySpawnData[i].Delay,
                waveNumber)
                .AttachExternalCancellation(_spawnCancellationTokenSource.Token);
        }

        await UniTask.WaitUntil(() => _unitModule.EnemyCount == 0)
            .AttachExternalCancellation(_spawnCancellationTokenSource.Token);

        _analyticsModule.SendWaveCompleteEvent(waveNumber + 1);
    }

    private async UniTask SpawnWithDelay(EnemyType enemyType, EnemyID enemyID, float delay, int waveNumber)
    {
        await UniTask.Delay(TimeSpan.FromSeconds(delay))
            .AttachExternalCancellation(_spawnCancellationTokenSource.Token);
        Spawn(enemyType, enemyID, waveNumber);
    }

    private void Spawn(EnemyType enemyType, EnemyID enemyID, int waveNumber)
    {
        var enemyParameters = _regularEnemyParametersProgression.GetEnemyCharacteristics(enemyType, enemyID, waveNumber);
        var enemyTemplate = _resourceLoader.Load<Enemy>(enemyParameters.PrefabName);
        var enemy = _objectResolver.Instantiate(enemyTemplate, GetRandomSpawnPoint().transform.position, Quaternion.identity, transform);
        enemy.SetParameters(enemyParameters);

        _enemySpawnEventPublisher.Publish(new EnemySpawnEvent(enemy));
    }

    private SpawnPoint GetRandomSpawnPoint()
    {
        var spawnPoints = new List<SpawnPoint>(_spawnPoints);
        spawnPoints.RemoveAll(x => _lastSpawnPoint.Contains(x));

        var randomSpawnPoint = spawnPoints[UnityEngine.Random.Range(0, spawnPoints.Count)];
        _lastSpawnPoint.Enqueue(randomSpawnPoint);
        if (_lastSpawnPoint.Count > _lastSpawnPointCount)
        {
            _lastSpawnPoint.Dequeue();
        }

        return randomSpawnPoint;
    }

    public void Handle(HeroDieEvent message)
    {
        _spawnCancellationTokenSource.Cancel();
    }

    public void Dispose()
    {
        _heroDieEventSubscribe.Dispose();
    }
}