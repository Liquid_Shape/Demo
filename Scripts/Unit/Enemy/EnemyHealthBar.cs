using UnityEngine;
using UnityEngine.UI;

public class EnemyHealthBar : MonoBehaviour
{
    [SerializeField] private Image _healthLine;
    [SerializeField] private Image _damageLine;
    [Space(10)]
    [SerializeField] private float _damageLineSpeed;

    private void Awake()
    {
        gameObject.SetActive(false);
    }

    public void SetFill(float value)
    {
        gameObject.SetActive(value < 1f && value > 0f);
        _healthLine.fillAmount = value;
    }

    private void Update()
    {
        _damageLine.fillAmount = Mathf.Lerp(_damageLine.fillAmount, _healthLine.fillAmount, _damageLineSpeed * Time.deltaTime);
        transform.rotation = Quaternion.LookRotation(Camera.main.transform.forward);
    }
}
