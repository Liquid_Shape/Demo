using Cysharp.Threading.Tasks;
using System;
using UnityEngine;

public class EnemyAnimator : MonoBehaviour
{
    [SerializeField] private Animator _animator;

    private string _attackAnimationKey = "on_attack";
    private string _moveAnimationKey = "move_speed";
    private string _hitAnimationKey = "on_hit";
    private string _deathAnimationKey = "is_dead";
    private string _deathWithOverDamageAnimationKey = "is_dead_with_over_damage";

    private UniTaskCompletionSource _attackTaskCompletionSource = new UniTaskCompletionSource();

    public async UniTask PlayAttackAnimationWithAction(Action attackAction)
    {
        _animator.SetTrigger(_attackAnimationKey); // TODO: Hashes
        await _attackTaskCompletionSource.Task;
        attackAction();
    }

    public void PlayMoveAnimation(float value)
    {
        _animator.SetFloat(_moveAnimationKey, value); // TODO: Hashes
    }

    public void PlayDeathAnimation(bool isDeathWithOverDamage)
    {
        _animator.SetBool(isDeathWithOverDamage ? _deathWithOverDamageAnimationKey : _deathAnimationKey, true); // TODO: Hashes
    }

    public void PlayHitAnimation()
    {
        _animator.SetTrigger(_hitAnimationKey); // TODO: Hashes
    }


    private void Attack() // Call from Animator 
    {
        _attackTaskCompletionSource.TrySetResult();
    }
}