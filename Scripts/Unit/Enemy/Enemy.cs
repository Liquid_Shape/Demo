using MessagePipe;
using UnityEngine;
using VContainer;

public struct EnemyDieEvent
{
    public readonly Enemy Enemy;
    public readonly bool DieWithOverDamage;

    public EnemyDieEvent(Enemy enemy, bool dieWithOverDamage)
    {
        Enemy = enemy;
        DieWithOverDamage = dieWithOverDamage;
    }
}

public struct EnemyTakeHitEvent
{
    public readonly Enemy Enemy;
    public readonly Damage Damage;

    public EnemyTakeHitEvent(Enemy enemy, Damage damage)
    {
        Enemy = enemy;
        Damage = damage;
    }
}

public class Enemy : MonoBehaviour
{
    [SerializeField] private UnitAI _ai;
    [SerializeField] private EnemyAnimator _animator;
    [SerializeField] private EnemyHealthBar _enemyHealthBar;
    [SerializeField] private Transform _hitPoint;
    [SerializeField] private Transform _deathEffectPoint;
    [SerializeField] private Transform _hitEffectPoint;
    [Space(10)]
    [SerializeField] private string _hitSoundName;
    [SerializeField] private string _dieSoundName;
    [Space(10)]
    [SerializeField] private string _deathEffectName;
    [SerializeField] private string _hitEffectName;

    private float _maxHealth;
    private float _health;

    public EnemyAnimator Animator => _animator;
    public Transform HitPoint => _hitPoint;
    public Transform DeathEffectPoint => _deathEffectPoint;
    public Transform HitEffectPoint => _hitEffectPoint;
    public string HitSoundName => _hitSoundName;
    public string DieSoundName => _dieSoundName;
    public string DeathEffectName => _deathEffectName;
    public string HitEffectName => _hitEffectName;
    public EnemyID EnemyID { get; private set; }
    public EnemyType EnemyType { get; private set; }

    private GlobalData _globalData;
    private Hero _hero;
    private IPublisher<EnemyDieEvent> _enemyDieEventPublisher;
    private IPublisher<EnemyTakeHitEvent> _enemyTakeHitEventPublisher;

    public bool IsAlive { get; private set; } = true;

    [Inject]
    public void Initialize(DefinitionsProvider definitionsProvider, Hero hero,
        IPublisher<EnemyDieEvent> enemyDieEventPublisher,
        IPublisher<EnemyTakeHitEvent> enemyTakeHitEventPublisher)
    {
        _globalData = definitionsProvider.Get<GlobalData>();
        _hero = hero;
        _enemyDieEventPublisher = enemyDieEventPublisher;
        _enemyTakeHitEventPublisher = enemyTakeHitEventPublisher;
    }

    public void SetParameters(EnemyCharacteristics parameters)
    {
        EnemyID = parameters.EnemyID;
        EnemyType = parameters.EnemyType;        
        transform.forward = new Vector3(_hero.transform.position.x, transform.position.y, _hero.transform.position.z) - transform.position;
        _health = _maxHealth = parameters.Health;
        _ai.Initialize(this, _hero, parameters);
        _ai.Activate();
    }

    public void SetDamage(Damage damage)
    {
        _enemyTakeHitEventPublisher.Publish(new EnemyTakeHitEvent(this, damage));
        _animator.PlayHitAnimation();
        _health -= damage.Value;
        _enemyHealthBar.SetFill(_health / _maxHealth);

        if (_health <= 0f)
        {
            SetDeath(damage.Value >= _maxHealth * _globalData.OverDamageMultiplier);
        }
    }

    private void SetDeath(bool isDeathWithOverDamage)
    {
        _enemyDieEventPublisher.Publish(new EnemyDieEvent(this, isDeathWithOverDamage));
        _animator.PlayDeathAnimation(isDeathWithOverDamage);

        _ai.Deactivate();
        IsAlive = false;
        Destroy(gameObject, 3f);
    }
}