using UnityEngine;

public class IdleBehaviour : EnemyBehaviour
{
    public override bool TryGetNextBehaviour(out EnemyBehaviour nextBehaviour)
    {
        nextBehaviour = null;

        return nextBehaviour != null;
    }
    
    protected override void OnStart()
    {
        
    }
    
    protected override void OnFinished()
    {
        
    }

    protected override void OnUpdate()
    {
        
    }
    
    protected override void OnFixedUpdate()
    {
        
    }
}