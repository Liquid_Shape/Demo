using UnityEngine;
using Cysharp.Threading.Tasks;
using System;
using System.Threading;

public class AttackBehaviour : EnemyBehaviour, IBehaviourWithAttack
{
    [SerializeField] private EnemyBehaviour _cancelBehaviour;

    public float AttackDamage { get; set; }
    public float AttackSpeed { get; set; }   
    
    public float RotationToTargetSpeed { get; set; }

    public override bool TryGetNextBehaviour(out EnemyBehaviour nextBehaviour)
    {
        nextBehaviour = null;

        if (!Hero.IsAlive)
        {
            nextBehaviour = _cancelBehaviour;
        }

        return nextBehaviour != null;
    }

    private CancellationTokenSource _attackCancellationTokenSource = new CancellationTokenSource();

    protected override void OnStart()
    {        
        AttackTask().Forget();

        async UniTaskVoid AttackTask()
        {
            while (true)
            {
                await UniTask.WhenAll(
                    Enemy.Animator.PlayAttackAnimationWithAction(AttackAction),
                    UniTask.Delay(TimeSpan.FromSeconds(AttackSpeed)))
                    .AttachExternalCancellation(_attackCancellationTokenSource.Token);
            }            
        }
    }

    private void AttackAction()
    {
        Hero.SetDamage(AttackDamage);
    }
    
    protected override void OnFinished()
    {
        _attackCancellationTokenSource.Cancel();
    }

    protected override void OnUpdate()
    {
    }

    protected override void OnFixedUpdate()
    {

    }

    private void OnDestroy()
    {
        _attackCancellationTokenSource.Cancel();
    }
}