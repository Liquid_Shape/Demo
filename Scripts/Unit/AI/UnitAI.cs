using System.Linq;
using UnityEngine;

public class UnitAI : MonoBehaviour
{
    [SerializeField] private EnemyBehaviour[] _states;
    
    private EnemyBehaviour _currentState;

    public bool IsActive { get; private set; }
    
    private void Update()
    {
        if (!IsActive)
        {
            return;
        }

        if (_currentState.TryGetNextBehaviour(out var nextBehaviour))
        {
            _currentState.Deactivate();
            nextBehaviour.Activate();
            _currentState = nextBehaviour;
        }
    }

    public void Initialize(Enemy enemy, Hero hero, EnemyCharacteristics enemyParameters)
    {
        foreach (var state in _states)
        {
            state.Initialize(enemy, hero);
            if (state is IBehaviourWithMovement behaviourWithMovement)
            {
                behaviourWithMovement.MoveSpeed = enemyParameters.MoveSpeed;
                behaviourWithMovement.StopDistance = enemyParameters.StopDistance;
            }
            if (state is IBehaviourWithAttack behaviourWithAttack)
            {
                behaviourWithAttack.AttackDamage = enemyParameters.AttackDamage;
                behaviourWithAttack.AttackSpeed = enemyParameters.AttackSpeed;
            }
        }

        if (_states.Length > 0)
        {
            _currentState = _states.First();
        }
    }

    public void Activate()
    {
        _currentState.Activate();
        IsActive = true;
    }
    
    public void Deactivate()
    {
        _currentState.Deactivate();
        IsActive = false;
    }
}