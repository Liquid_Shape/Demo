using UnityEngine;

public interface IBehaviourWithMovement
{
    float MoveSpeed { get; set; }
    float StopDistance { get; set; }
}

public interface IBehaviourWithAttack
{
    public float AttackDamage { get; set; }
    public float AttackSpeed { get; set; }    
}

public abstract class EnemyBehaviour : MonoBehaviour
{
    protected bool _isActive;

    public Enemy Enemy { get; private set; }
    public Hero Hero { get; private set; }

    private void Update()
    {
        if (_isActive)
        {
            OnUpdate();
        }
    }
    
    private void FixedUpdate()
    {
        if (_isActive)
        {
            OnFixedUpdate();
        }
    }

    public void Initialize(Enemy enemy, Hero hero)
    {
        Enemy = enemy;
        Hero = hero;
    }

    public void Activate()
    {
        _isActive = true;
        OnStart();
    }

    public void Deactivate()
    {
        _isActive = false;
        OnFinished();
    }
    
    public abstract bool TryGetNextBehaviour(out EnemyBehaviour nextBehaviour);
    protected abstract void OnStart();
    protected abstract void OnFinished();
    protected abstract void OnUpdate();
    protected abstract void OnFixedUpdate();
}