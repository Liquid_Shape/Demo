using UnityEngine;

public class MoveToHeroBehaviour : EnemyBehaviour, IBehaviourWithMovement
{
    [SerializeField] private EnemyBehaviour _cancelBehaviour;
    [SerializeField] private EnemyBehaviour _nextBehaviour;

    public float MoveSpeed { get; set; }
    public float StopDistance { get; set; }

    public override bool TryGetNextBehaviour(out EnemyBehaviour nextBehaviour)
    {
        nextBehaviour = null;
        
        if (!Hero.IsAlive)
        {
            nextBehaviour = _cancelBehaviour;
        }

        else if (Vector3.Distance(Enemy.transform.position, new Vector3(Hero.transform.position.x, Enemy.transform.position.y, Hero.transform.position.y)) <= StopDistance)
        {
            nextBehaviour = _nextBehaviour;
        }

        return nextBehaviour != null;
    }

    protected override void OnStart()
    {
        Enemy.Animator.PlayMoveAnimation(1f);
    }

    protected override void OnFinished()
    {
        Enemy.Animator.PlayMoveAnimation(0f);
    }

    protected override void OnUpdate()
    {
        Enemy.transform.position = Vector3.MoveTowards(Enemy.transform.position, new Vector3(Hero.transform.position.x, Enemy.transform.position.y, Hero.transform.position.y), MoveSpeed * Time.deltaTime);
    }
    
    protected override void OnFixedUpdate()
    {

    }
}