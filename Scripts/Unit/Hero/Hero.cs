using Cysharp.Threading.Tasks;
using MessagePipe;
using System;
using UnityEngine;
using VContainer; 

public struct HeroStartDieEvent 
{
    public readonly Hero Hero;

    public HeroStartDieEvent(Hero hero)
    {
        Hero = hero;
    }
}
public struct HeroDieEvent { }

public struct HeroTakeHitEvent
{
    public readonly Hero Hero;

    public HeroTakeHitEvent(Hero hero)
    {
        Hero = hero;
    }
}

public struct HeroAttackEvent
{
    public readonly Hero Hero;

    public HeroAttackEvent(Hero hero)
    {
        Hero = hero;
    }
}

public class Hero : MonoBehaviour, IDisposable,
    IMessageHandler<HeroParameterUpdateEvent>
{
    [SerializeField] private HeroAnimator _animator;
    [SerializeField] private Projectile _projectile;
    [SerializeField] private Transform _body;
    [SerializeField] private Transform _attackPoint;
    [SerializeField] private Transform _projectileSpawnPoint;
    [SerializeField] private Transform _deathEffectPoint;
    [Space(10)]
    [SerializeField] private string _attackSoundName;
    [SerializeField] private string _hitSoundName;
    [SerializeField] private string _dieSoundName;
    [Space(10)]
    [SerializeField] private string _deathEffectName;
    [SerializeField] private string _attackEffectName;

    private HeroModule _heroModule;
    private AnalyticsModule _analyticsModule;

    private HeroBehaviour _heroBehaviour;
    private HeroHealth _health;

    private IPublisher<HeroDieEvent> _heroDieEventPublisher;
    private IPublisher<HeroStartDieEvent> _heroStartDieEventPublisher;
    private IPublisher<HeroTakeHitEvent> _heroTakeHitEventPublisher;
    private IDisposable _heroParameterUpdateEventSubscribe;

    public bool IsAlive { get; private set; } = true;
    public Projectile Projectile => _projectile;
    public HeroAnimator Animator => _animator;
    public Transform AttackPoint => _attackPoint;
    public Transform ProjectileSpawnPoint => _projectileSpawnPoint;
    public Transform DeathEffectPoint => _deathEffectPoint;
    public string AttackSoundName => _attackSoundName;
    public string HitSoundName => _hitSoundName;
    public string DieSoundName => _dieSoundName;
    public string DeathEffectName => _deathEffectName;
    public string AttackEffectName => _attackEffectName;

    [Inject]
    public void Initialize(HeroModule heroModule, UnitModule unitModule, AnalyticsModule analyticsModule,
        IPublisher<HeroStartDieEvent> heroStartDieEventPublisher,
        IPublisher<HeroDieEvent> heroDieEventPublisher,
        IPublisher<HeroTakeHitEvent> heroTakeHitEventPublisher,
        IPublisher<HeroAttackEvent> heroAttackEventPublisher,
        IPublisher<HeroChangeHealthEvent> heroChangeHealthEventPublisher,
        ISubscriber<HeroParameterUpdateEvent> heroParameterUpdateEventSubscriber)
    {
        _heroDieEventPublisher = heroDieEventPublisher;
        _heroStartDieEventPublisher = heroStartDieEventPublisher;
        _heroTakeHitEventPublisher = heroTakeHitEventPublisher;

        _heroModule = heroModule;
        _analyticsModule = analyticsModule;
        _heroBehaviour = new HeroBehaviour(this, heroModule, unitModule, heroAttackEventPublisher); // TODO Inject
        _health = new HeroHealth(this, heroChangeHealthEventPublisher); // TODO Inject
        _health.AddMaxHealth(_heroModule.GetHealth());
        _heroParameterUpdateEventSubscribe = heroParameterUpdateEventSubscriber.Subscribe(this);
    }

    private void Awake()
    {
        _health.SendChangeHealthEvent();
    }

    public void SetDamage(float damage)
    {        
        _health.SetDamage(damage);
        _heroTakeHitEventPublisher.Publish(new HeroTakeHitEvent(this));

        if (_health.Value <= 0f)
        {
            SetDeath();
        }
    }

    private void Update()
    {
        _body.rotation = Quaternion.Lerp(_body.rotation, _attackPoint.rotation, _heroModule.GetRotationSpeed() * Time.deltaTime);
        _heroBehaviour.OnUpdate();
    }

    private void SetDeath()
    {
        IsAlive = false;
        _heroBehaviour.Deactivate();
        _health.Deactivate();
        _heroStartDieEventPublisher.Publish(new HeroStartDieEvent(this));
        _animator.PlayDeathAnimationWithAction(() => 
        {
            _heroDieEventPublisher.Publish(new HeroDieEvent());
        }).Forget();
        
        _analyticsModule.SendHeroDieEvent(); // TODO: Move to Analitics Provider
    }

    public void Handle(HeroParameterUpdateEvent message)
    {
        if (message.Parameter == HeroParameterType.Health)
        {
            _health.AddMaxHealth(message.NewParameter.Value - _health.MaxValue);
        }
        else if (message.Parameter == HeroParameterType.HealthRegeneration)
        {
            _health.SetRegeneration(message.NewParameter.Value);
        }
    }

    public void Dispose()
    {
        _heroParameterUpdateEventSubscribe.Dispose();
    }
}