using Cysharp.Threading.Tasks;
using MessagePipe;
using System;
using System.Threading;
using UnityEngine;

public class HeroBehaviour
{
    private Hero _hero;
    private HeroModule _heroModule;
    private UnitModule _unitModule;

    IPublisher<HeroAttackEvent> _heroAttackEventPublisher;

    private Enemy _target;
    private bool _isReady = true;

    private CancellationTokenSource _attackCancellationTokenSource = new CancellationTokenSource();

    public HeroBehaviour(Hero hero, HeroModule heroModule, UnitModule unitModule,
        IPublisher<HeroAttackEvent> heroAttackEventPublisher)
    {
        _hero = hero;
        _heroModule = heroModule;
        _unitModule = unitModule;
        _heroAttackEventPublisher = heroAttackEventPublisher;
        _attackCancellationTokenSource.RegisterRaiseCancelOnDestroy(hero);
    }

    public void Deactivate()
    {
        _attackCancellationTokenSource.Cancel();
    }

    public void OnUpdate()
    {
        if (_target == null || !_target.IsAlive)
        {
            _target = _unitModule.GetNearestEnemyInRangeAtPoint(_hero.transform.position, _heroModule.GetRange());
        }
        else
        {
            _hero.AttackPoint.LookAt(new Vector3(_target.transform.position.x, _hero.AttackPoint.position.y, _target.transform.position.z));
            if (_isReady)
            {
                AttackTask(1 / _heroModule.GetAttackSpeed()).Forget();
            }
        }
    }

    private async UniTaskVoid AttackTask(float attackTime)
    {
        _isReady = false;
        await UniTask.WhenAll(
                _hero.Animator.PlayAttackAnimationWithAction(AttackAction),
                UniTask.Delay(TimeSpan.FromSeconds(attackTime)))
                .AttachExternalCancellation(_attackCancellationTokenSource.Token);
        _isReady = true;
    }

    private void AttackAction()
    {
        if (_target != null && _target.IsAlive)
        {                      
            InstantiateProjectiles(_target);
            _heroAttackEventPublisher.Publish(new HeroAttackEvent(_hero));
        }
    }

    private void InstantiateProjectiles(Enemy target)
    {
        var projectile = GameObject.Instantiate(_hero.Projectile, _hero.ProjectileSpawnPoint.position, Quaternion.identity);
        projectile.Initialize(target, Damage, _heroModule.GetProjectileSpeed());
    }

    private void Damage(Enemy target)
    {
        target.SetDamage(_heroModule.GetDamage());
    }
}