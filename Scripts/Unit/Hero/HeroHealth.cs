using Cysharp.Threading.Tasks;
using MessagePipe;
using System;
using System.Threading;
using UnityEngine;

public struct HeroChangeHealthEvent
{
    public readonly float CurentHealth;
    public readonly float MaxHealth;
    public HeroChangeHealthEvent(float curentHealth, float maxHealth)
    {
        CurentHealth = curentHealth;
        MaxHealth = maxHealth;
    }
}

public class HeroHealth
{
    private const float _regenerationTickTime = 0.1f;

    private IPublisher<HeroChangeHealthEvent> _heroChangeHealthEventPublisher;
    private CancellationTokenSource _regenerationTokenSource = new CancellationTokenSource();

    public float Value { get; private set; }
    public float Regeneration { get; private set; }
    public float MaxValue { get; private set; }

    public HeroHealth(Hero hero,
        IPublisher<HeroChangeHealthEvent> heroChangeHealthEventPublisher)
    {
        _heroChangeHealthEventPublisher = heroChangeHealthEventPublisher;
        _regenerationTokenSource.RegisterRaiseCancelOnDestroy(hero);

        RegenerationTask().Forget();
    }

    public void SetDamage(float damage)
    {
        damage = damage < 0f ? 0f : damage;
        Value -= damage;
        Value = Mathf.Clamp(Value, 0f, MaxValue);
        SendChangeHealthEvent();
    }

    public void SetHeal(float heal)
    {
        heal = heal < 0f ? 0f : heal;
        Value += heal;
        Value = Mathf.Clamp(Value, 0f, MaxValue);
        SendChangeHealthEvent();
    }

    public void SetRegeneration(float regeneration)
    {
        regeneration = regeneration < 0f ? 0f : regeneration;
        Regeneration = regeneration;
    }

    public void AddMaxHealth(float addedHealth)
    {
        addedHealth = addedHealth < 0f ? 0f : addedHealth;
        MaxValue += addedHealth;
        Value += addedHealth;
        SendChangeHealthEvent();
    }

    public void Deactivate()
    {
        _regenerationTokenSource.Cancel();
    }

    private async UniTaskVoid RegenerationTask()
    {
        while (true)
        {
            await UniTask.Delay(TimeSpan.FromSeconds(_regenerationTickTime)).AttachExternalCancellation(_regenerationTokenSource.Token);
            if (Value < MaxValue)
            {
                SetHeal(Regeneration * _regenerationTickTime);
            }
        }
    }

    public void SendChangeHealthEvent()
    {
        _heroChangeHealthEventPublisher.Publish(new HeroChangeHealthEvent(Value, MaxValue));
    }
}