using Cysharp.Threading.Tasks;
using System;
using UnityEngine;

public class HeroAnimator : MonoBehaviour
{
    [SerializeField] private Animator _animator;

    private string _attackAnimationKey = "is_attack";
    private string _deathAnimationKey = "is_dead";

    private UniTaskCompletionSource _attackTaskCompletionSource = new UniTaskCompletionSource();
    private UniTaskCompletionSource _deathTaskCompletionSource = new UniTaskCompletionSource();

    public async UniTask PlayAttackAnimationWithAction(Action attackAction)
    {
        _animator.SetTrigger(_attackAnimationKey); // TODO: Hashes
        await _attackTaskCompletionSource.Task;
        attackAction();
    }

    public async UniTask PlayDeathAnimationWithAction(Action deathAction)
    {
        _animator.SetBool(_deathAnimationKey, true); // TODO: Hashes
        await _deathTaskCompletionSource.Task;
        deathAction();
    }

    private void Attack() // Call from Animator 
    {
        _attackTaskCompletionSource.TrySetResult();
    }

    private void Death() // Call from Animator 
    {
        _deathTaskCompletionSource.TrySetResult();
    }
}