public class Damage
{
    public readonly float Value;
    public readonly bool IsCritical;

    public Damage(float value, bool isCritical)
    {
        Value = value;
        IsCritical = isCritical;
    }
}