using System;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private bool _isActive;
    private Enemy _target;
    private Action<Enemy> _attackAction;
    private float _speed;

    public void Initialize(Enemy target, Action<Enemy> attackAction, float speed)
    {
        _isActive = true;
        _target = target;
        _attackAction = attackAction;
        _speed = speed;
    }

    private void Update()
    {
        if (_isActive)
        {
            transform.position = Vector3.MoveTowards(transform.position, _target.HitPoint.position, _speed * Time.deltaTime);
            transform.LookAt(_target.HitPoint);
            if (Vector3.Distance(transform.position, _target.HitPoint.position) <= 0.1f)
            {
                _attackAction(_target);
                Destroy(gameObject);
            }
        }
    }
}