using MessagePipe;
using System;
using VContainer.Unity;

public class CameraEventHandler : IStartable, IDisposable,
    IMessageHandler<HeroTakeHitEvent>,
    IMessageHandler<HeroStartDieEvent>,
    IMessageHandler<EnemyDieEvent>,
    IMessageHandler<EnemyTakeHitEvent>
{
    private CameraEffectPlayer _cameraEffectPlayer;
    private CameraData _cameraData;
    private IDisposable _subscribes;

    public CameraEventHandler(CameraEffectPlayer cameraEffectPlayer, DefinitionsProvider definitionsProvider,
        ISubscriber<HeroTakeHitEvent> heroTakeHitEventSubscriber,
        ISubscriber<HeroStartDieEvent> heroStartDieEventSubscriber,
        ISubscriber<EnemyDieEvent> enemyDieEventSubscriber,
        ISubscriber<EnemyTakeHitEvent> enemyTakeHitEventSubscriber)
    {
        _cameraEffectPlayer = cameraEffectPlayer;
        _cameraData = definitionsProvider.Get<GlobalData>().CameraData; 
        var bag = DisposableBag.CreateBuilder();

        heroTakeHitEventSubscriber.Subscribe(this).AddTo(bag);
        heroStartDieEventSubscriber.Subscribe(this).AddTo(bag);
        enemyDieEventSubscriber.Subscribe(this).AddTo(bag);
        enemyTakeHitEventSubscriber.Subscribe(this).AddTo(bag);

        _subscribes = bag.Build();
    }

    public void Start()
    {
        
    }

    public void Dispose()
    {
        _subscribes.Dispose();
    }

    public void Handle(HeroTakeHitEvent message)
    {
        _cameraEffectPlayer.Shake(_cameraData.MiddleShakeValue, _cameraData.MiddleShakeTime);
    }

    public void Handle(HeroStartDieEvent message)
    {
        _cameraEffectPlayer.Shake(_cameraData.BigShakeValue, _cameraData.BigShakeTime);
    }

    public void Handle(EnemyDieEvent message)
    {
        if (message.DieWithOverDamage)
        {
            _cameraEffectPlayer.Shake(_cameraData.SmallShakeValue, _cameraData.SmallShakeTime);
        }
    }

    public void Handle(EnemyTakeHitEvent message)
    {
        if (message.Damage.IsCritical)
        {
            _cameraEffectPlayer.Shake(_cameraData.SmallShakeValue, _cameraData.SmallShakeTime);
        }
    }
}