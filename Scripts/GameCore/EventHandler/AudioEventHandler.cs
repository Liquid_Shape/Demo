using MessagePipe;
using System;
using VContainer.Unity;

public class AudioEventHandler : IStartable, IDisposable,
    IMessageHandler<UIButtonClickEvent>,
    IMessageHandler<HeroParameterUpdateEvent>,
    IMessageHandler<HeroTakeHitEvent>,
    IMessageHandler<HeroStartDieEvent>,
    IMessageHandler<HeroAttackEvent>,
    IMessageHandler<EnemyDieEvent>,
    IMessageHandler<EnemyTakeHitEvent>,
    IMessageHandler<EnemySpawnEvent>
{
    private AudioPlayer _audioPlayer;
    private IDisposable _subscribes;

    public AudioEventHandler(AudioPlayer audioPlayer,
        ISubscriber<UIButtonClickEvent> uiButtonClickEventSubscriber,
        ISubscriber<HeroParameterUpdateEvent> heroParameterUpdateEventSubscriber,
        ISubscriber<HeroTakeHitEvent> heroTakeHitEventSubscriber,
        ISubscriber<HeroStartDieEvent> heroStartDieEventSubscriber,
        ISubscriber<HeroAttackEvent> heroAttackEventSubscriber,
        ISubscriber<EnemyDieEvent> enemyDieEventSubscriber,
        ISubscriber<EnemyTakeHitEvent> enemyTakeHitEventSubscriber,
        ISubscriber<EnemySpawnEvent> enemySpawnEventSubscriber)
    {
        _audioPlayer = audioPlayer;
        var bag = DisposableBag.CreateBuilder();

        uiButtonClickEventSubscriber.Subscribe(this).AddTo(bag);
        heroParameterUpdateEventSubscriber.Subscribe(this).AddTo(bag);
        heroTakeHitEventSubscriber.Subscribe(this).AddTo(bag);
        heroStartDieEventSubscriber.Subscribe(this).AddTo(bag);
        heroAttackEventSubscriber.Subscribe(this).AddTo(bag);
        enemyDieEventSubscriber.Subscribe(this).AddTo(bag);
        enemyTakeHitEventSubscriber.Subscribe(this).AddTo(bag);
        enemySpawnEventSubscriber.Subscribe(this).AddTo(bag);

        _subscribes = bag.Build();
    }

    public void Start()
    {
        _audioPlayer.PlayAmbient();
    }

    public void Dispose()
    {
        _subscribes.Dispose();
    }

    public void Handle(EnemyDieEvent message)
    {
        _audioPlayer.Play(message.Enemy.DieSoundName, AudioType.Sound, true);
    }

    public void Handle(EnemyTakeHitEvent message)
    {
        _audioPlayer.Play(message.Enemy.HitSoundName, AudioType.Sound, true);
    }

    public void Handle(UIButtonClickEvent message)
    {
        _audioPlayer.PlayUIClickSound();
    }

    public void Handle(HeroParameterUpdateEvent message)
    {
        if (message.NeedSound)
        {
            _audioPlayer.PlayUpgradeCoreParameterSound();
        }
    }

    public void Handle(EnemySpawnEvent message)
    {
        if (message.Enemy.EnemyType == EnemyType.Boss)
        {
            _audioPlayer.PlayBossSpawnSound();
        }
    }

    public void Handle(HeroTakeHitEvent message)
    {
        _audioPlayer.Play(message.Hero.HitSoundName, AudioType.Sound, true);
    }

    public void Handle(HeroStartDieEvent message)
    {
        _audioPlayer.Play(message.Hero.DieSoundName);
    }

    public void Handle(HeroAttackEvent message)
    {
        _audioPlayer.Play(message.Hero.AttackSoundName, AudioType.Sound, true);
    }
}