using MessagePipe;
using System;
using VContainer.Unity;

public class EffectsEventHandler : IStartable, IDisposable,
    IMessageHandler<HeroStartDieEvent>,
    IMessageHandler<HeroAttackEvent>,
    IMessageHandler<EnemyTakeHitEvent>,
    IMessageHandler<EnemyDieEvent>
{
    private EffectPlayer _effectPlayer;
    private IDisposable _subscribes;

    public EffectsEventHandler(EffectPlayer effectPlayer,
        ISubscriber<HeroStartDieEvent> heroStartDieEventSubscriber,
        ISubscriber<HeroAttackEvent> heroAttackEventSubscriber,
        ISubscriber<EnemyTakeHitEvent> enemyTakeHitEventSubscriber,
        ISubscriber<EnemyDieEvent> enemyDieEventSubscriber)
    {
        _effectPlayer = effectPlayer;
        var bag = DisposableBag.CreateBuilder();

        heroStartDieEventSubscriber.Subscribe(this).AddTo(bag);
        heroAttackEventSubscriber.Subscribe(this).AddTo(bag);
        enemyTakeHitEventSubscriber.Subscribe(this).AddTo(bag);
        enemyDieEventSubscriber.Subscribe(this).AddTo(bag);

        _subscribes = bag.Build();
    }

    public void Start()
    {
        
    }

    public void Dispose()
    {
        _subscribes.Dispose();
    }

    public void Handle(HeroStartDieEvent message)
    {
        _effectPlayer.CreateEffectOnContainer(message.Hero.DeathEffectName, message.Hero.DeathEffectPoint);
    }

    public void Handle(HeroAttackEvent message)
    {
        _effectPlayer.CreateEffectAtPoint(message.Hero.AttackEffectName,
            message.Hero.ProjectileSpawnPoint.position,
            message.Hero.ProjectileSpawnPoint.transform.rotation);
    }

    public void Handle(EnemyTakeHitEvent message)
    {
        _effectPlayer.CreateEffectAtPoint(message.Enemy.HitEffectName,
            message.Enemy.HitEffectPoint.position,
            message.Enemy.HitEffectPoint.transform.rotation);
    }

    public void Handle(EnemyDieEvent message)
    {
        _effectPlayer.CreateEffectAtPoint(message.Enemy.DeathEffectName,
            message.Enemy.DeathEffectPoint.position,
            message.Enemy.DeathEffectPoint.transform.rotation);
    }
}