using VContainer;
using VContainer.Unity;

public class GameSceneContext : LifetimeScope
{
    protected override void Configure(IContainerBuilder builder)
    {
        builder.Register<UnitModule>(Lifetime.Scoped);
        builder.RegisterEntryPoint<HeroModule>().AsSelf();
        builder.RegisterEntryPoint<CameraEventHandler>().AsSelf();
        builder.RegisterEntryPoint<EffectPlayer>().AsSelf();
        builder.RegisterEntryPoint<EffectsEventHandler>().AsSelf();
        builder.RegisterComponentInHierarchy<Hero>();
        builder.RegisterComponentInHierarchy<EnemySpawner>();
        builder.RegisterComponentInHierarchy<UIManager>();
        builder.RegisterComponentInHierarchy<CameraEffectPlayer>();
    }
}