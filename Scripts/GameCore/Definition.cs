using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDefinition {}
public interface IDefinitionSingle : IDefinition {}
public interface IDefinitionById : IDefinition
{
    public string Id { get; }
}
public interface IDefinitionsProvider
{
    public T Get<T>() where T : class, IDefinitionSingle;
    public T Get<T>(string id) where T : class, IDefinitionById;
    public bool TryGet<T>(string id, out T result) where T : class, IDefinitionById;
    public IReadOnlyList<T> GetAllOf<T>() where T : class, IDefinition;
    public IReadOnlyList<IDefinition> GetAll();
}