using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class ProgressModule
{
    private const string _fileName = "gamedata";

    private ProgressData _data;

    public static string FilePath => string.Concat(Application.persistentDataPath, Path.AltDirectorySeparatorChar, _fileName, ".json");

    public ProgressData Data => _data;

    public ProgressModule()
    {
        if (!TryLoad())
        {
            _data = new ProgressData(0);
        }
    }

    public void Save()
    {
        var serializedData = JsonConvert.SerializeObject(_data);
        File.WriteAllText(FilePath, serializedData);
        
    }

    private bool TryLoad()
    {
        if (!File.Exists(FilePath))
        {
            return false;
        }

        var text = File.ReadAllText(FilePath);        
        _data = JsonConvert.DeserializeObject<ProgressData>(text);

        return true;
    }

#if UNITY_EDITOR
    [MenuItem("Utils/Progress/Delete Save")]
    public static void RemoveSave()
    {
        if (File.Exists(FilePath))
        {
            File.Delete(FilePath);
        }
    }
#endif
}

[System.Serializable]
public class ProgressData
{
    public int Level;

    public ProgressData(int level)
    {
        Level = level;
    }
}