using System.Collections.Generic;
using UnityEngine;
using VContainer.Unity;

public class EffectPlayer : IStartable
{
    private const float _effectBaseLifeTime = 3f;

    private ResourceLoader _resourceLoader;
    private Dictionary<string, ParticleSystem> _effects�ache = new Dictionary<string, ParticleSystem>();
    private Transform _effectsContainer;

    public EffectPlayer(ResourceLoader resourceLoader)
    {
        _resourceLoader = resourceLoader;
    }

    public void Start()
    {
        _effectsContainer = new GameObject("EffectsContainer").transform;
    }

    public void CreateEffectOnContainer(string effectName, Transform container, float lifeTime = _effectBaseLifeTime)
    {
        var effect = GameObject.Instantiate(GetEffect(effectName), container);
        GameObject.Destroy(effect, lifeTime); // TODO: Rework to ObjectPool
    }

    public void CreateEffectAtPoint(string effectName, Vector3 position, Quaternion rotation, float lifeTime = _effectBaseLifeTime)
    {
        var effectTemplate = GetEffect(effectName);
        var effect = GameObject.Instantiate(effectTemplate, position, rotation, _effectsContainer);
        GameObject.Destroy(effect, lifeTime); // TODO: Rework to ObjectPool
    }

    private ParticleSystem GetEffect(string effectName)
    {
        if (!_effects�ache.TryGetValue(effectName, out var effect))
        {
            effect = _resourceLoader.Load<ParticleSystem>(effectName);
            _effects�ache.Add(effectName, effect);
        }

        return effect;
    }
}