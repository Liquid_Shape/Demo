using MessagePipe;
using System;
using System.Collections.Generic;
using UnityEngine;

public class UnitModule : IMessageHandler<EnemySpawnEvent>, IMessageHandler<EnemyDieEvent>, IDisposable
{
    private List<Enemy> _enemies = new List<Enemy>();

    private AnalyticsModule _analyticsModule;

    private IDisposable _enemySpawnEventSubscribe;
    private IDisposable _enemyDieEventSubscribe;

    public int EnemyCount => _enemies.Count;

    public UnitModule(AnalyticsModule analyticsModule,
        ISubscriber<EnemySpawnEvent> enemySpawnEventSubscriber,
        ISubscriber<EnemyDieEvent> enemyDieEventSubscriber)
    {
        _analyticsModule = analyticsModule;
        _enemySpawnEventSubscribe = enemySpawnEventSubscriber.Subscribe(this);
        _enemyDieEventSubscribe = enemyDieEventSubscriber.Subscribe(this);
    }

    public void Handle(EnemySpawnEvent message)
    {
        _enemies.Add(message.Enemy);
    }

    public void Handle(EnemyDieEvent message)
    {
        _enemies.Remove(message.Enemy);
        if (message.Enemy.EnemyType == EnemyType.Boss)
        {
            _analyticsModule.SendBossCompleteEvent(message.Enemy.EnemyID.ToString());
        }
    }

    public Enemy GetNearestEnemyInRangeAtPoint(Vector3 point, float range)
    {
        Enemy nearest = null;

        var nearestDistance = float.MaxValue;        
        foreach (var enemy in _enemies)
        {
            if (!enemy.IsAlive)
            {
                continue;
            }

            var start = new Vector2(enemy.transform.position.x, enemy.transform.position.z);
            var finish = new Vector2(point.x, point.z);
            var sqrDistance = (start - finish).sqrMagnitude;
            if (sqrDistance < range * range && sqrDistance < nearestDistance)
            {
                nearestDistance = sqrDistance;
                nearest = enemy;
            }
        }

        return nearest;
    }

    public void Dispose()
    {
        _enemySpawnEventSubscribe.Dispose();
        _enemyDieEventSubscribe.Dispose();
    }
}