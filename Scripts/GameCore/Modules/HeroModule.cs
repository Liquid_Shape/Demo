using MessagePipe;
using System;
using System.Collections.Generic;
using VContainer.Unity;

public struct HeroParameterUpdateEvent
{
    public readonly HeroParameterType Parameter;
    public readonly HeroCharacteristicProgressionData NewParameter;
    public readonly HeroCharacteristicProgressionData NextParameter;
    public readonly bool NeedSound;

    public HeroParameterUpdateEvent(HeroParameterType parameter,
        HeroCharacteristicProgressionData newParameter,
        HeroCharacteristicProgressionData nextParameter,
        bool needSound)
    {
        Parameter = parameter;
        NewParameter = newParameter;
        NextParameter = nextParameter;
        NeedSound = needSound;
    }
}

public class HeroModule : IStartable, IDisposable,
    IMessageHandler<SpawnWaveStartEvent>,
    IMessageHandler<EnemyDieEvent>
{
    private HeroCharacteristicsProgression _progression;
    private CurrencyModule _currencyModule;
    private AnalyticsModule _analyticsModule;

    private Dictionary<HeroParameterType, int> _progressionLevelMap;
    private IPublisher<HeroParameterUpdateEvent> _heroParameterUpdateEventPublisher;

    private IDisposable _spawnWaveStartEventSubscribe;
    private IDisposable _enemyDieEventSubscribe;

    public HeroModule(DefinitionsProvider definitionsProvider, CurrencyModule currencyModule, AnalyticsModule analyticsModule,
        IPublisher<HeroParameterUpdateEvent> heroParameterUpdateEventPublisher,
        ISubscriber<SpawnWaveStartEvent> spawnWaveStartEventSubscriber,
        ISubscriber<EnemyDieEvent> enemyDieEventSubscriber)
    {
        _progression = definitionsProvider.Get<HeroCharacteristicsProgression>();
        _currencyModule = currencyModule;
        _analyticsModule = analyticsModule;
        _heroParameterUpdateEventPublisher = heroParameterUpdateEventPublisher;
        _spawnWaveStartEventSubscribe = spawnWaveStartEventSubscriber.Subscribe(this);
        _enemyDieEventSubscribe = enemyDieEventSubscriber.Subscribe(this);
        _progressionLevelMap = new Dictionary<HeroParameterType, int>()
        {
            {HeroParameterType.Damage, 0},
            {HeroParameterType.AttackSpeed, 0},
            {HeroParameterType.CriticalChance, 0},
            {HeroParameterType.CriticalDamage, 0},
            {HeroParameterType.Health, 0},
            {HeroParameterType.HealthRegeneration, 0},
            {HeroParameterType.ExpPerKill, 0},
            {HeroParameterType.ExpPerLevel, 0},
        };

        currencyModule.AddCurrency(CurrencyType.Exp, _progression.StartExp);
    }

    public void Start()
    {
        foreach (var parameterType in (HeroParameterType[])Enum.GetValues(typeof(HeroParameterType)))
        {
            SendUpdateEvent(parameterType, false);
        }
    }

    public float GetBaseDamage() => _progression.GetParameterCharacteristics(HeroParameterType.Damage, _progressionLevelMap[HeroParameterType.Damage]).Value;
    public float GetAttackSpeed() => _progression.GetParameterCharacteristics(HeroParameterType.AttackSpeed, _progressionLevelMap[HeroParameterType.AttackSpeed]).Value;
    public float GetCriticalChance() => _progression.GetParameterCharacteristics(HeroParameterType.CriticalChance, _progressionLevelMap[HeroParameterType.CriticalChance]).Value;
    public float GetCriticalMultiplier() => _progression.GetParameterCharacteristics(HeroParameterType.CriticalDamage, _progressionLevelMap[HeroParameterType.CriticalDamage]).Value;
    public float GetHealth() => _progression.GetParameterCharacteristics(HeroParameterType.Health, _progressionLevelMap[HeroParameterType.Health]).Value;
    public float GetRegeneration() => _progression.GetParameterCharacteristics(HeroParameterType.HealthRegeneration, _progressionLevelMap[HeroParameterType.HealthRegeneration]).Value;
    public float GetExpPerKill() => _progression.GetParameterCharacteristics(HeroParameterType.ExpPerKill, _progressionLevelMap[HeroParameterType.ExpPerKill]).Value;
    public float GetExpPerLevel() => _progression.GetParameterCharacteristics(HeroParameterType.ExpPerLevel, _progressionLevelMap[HeroParameterType.ExpPerLevel]).Value;
    public float GetProjectileSpeed() => _progression.ProjectileSpeed;
    public float GetRange() => _progression.Ranage;
    public float GetRotationSpeed() => _progression.RotationSpeed;

    public Damage GetDamage()
    {        
        var isCritical = UnityEngine.Random.Range(0f, 1f) <= GetCriticalChance();
        var damageValue = GetBaseDamage() * (isCritical ? GetCriticalMultiplier() : 1f);
        return new Damage(damageValue, isCritical);
    }

    public bool CanUpgradeParameter(HeroParameterType parameterType)
    {
        if (!HasNextProgression(parameterType))
        {
            return false;
        }

        var parameter = _progression.GetParameterCharacteristics(parameterType, _progressionLevelMap[parameterType] + 1);
        if (_currencyModule.GetCurrency(CurrencyType.Exp) < parameter.Cost)
        {
            return false;
        }

        return true;
    }

    public bool TryUpgradeParameter(HeroParameterType parameterType)
    {
        if (!HasNextProgression(parameterType))
        {
            return false;
        }

        var parameter = _progression.GetParameterCharacteristics(parameterType, _progressionLevelMap[parameterType] + 1);
        if (_currencyModule.TrySpendCurrency(CurrencyType.Exp, parameter.Cost))
        {
            _progressionLevelMap[parameterType]++;
            SendUpdateEvent(parameterType);
            _analyticsModule.SendCoreProgressionUpEvent(parameterType.ToString(), _progressionLevelMap[parameterType]);

            return true;
        }
       
        return false;       
    }    

    public void Handle(SpawnWaveStartEvent message)
    {
        _currencyModule.AddCurrency(CurrencyType.Exp, GetExpPerLevel());
    }

    public void Handle(EnemyDieEvent message)
    {
        _currencyModule.AddCurrency(CurrencyType.Exp, GetExpPerKill());
    }

    public void Dispose()
    {
        _spawnWaveStartEventSubscribe.Dispose();
        _enemyDieEventSubscribe.Dispose();
    }

    private bool HasNextProgression(HeroParameterType parameterType) => _progression.HasLevel(parameterType, _progressionLevelMap[parameterType] + 1);

    private void SendUpdateEvent(HeroParameterType parameterType, bool needSound = true)
    {
        _heroParameterUpdateEventPublisher.Publish(new HeroParameterUpdateEvent(
            parameterType,
            _progression.GetParameterCharacteristics(parameterType, _progressionLevelMap[parameterType]),
            _progression.HasLevel(parameterType, _progressionLevelMap[parameterType] + 1) ? _progression.GetParameterCharacteristics(parameterType, _progressionLevelMap[parameterType] + 1) : null,
            needSound));
    }
}