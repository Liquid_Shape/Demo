using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using VContainer.Unity;

public interface IAnaliticsServiceProvider
{
    void Initialize();
    void SendEvent(string eventName, params (string, string)[] parameters);
}

public class AnalyticsModule : IStartable
{
    private List<IAnaliticsServiceProvider> _services = new List<IAnaliticsServiceProvider>();

    private const string _customFirstOpenKey = "CustomFirstOpenKey";
    private const string _playTimeKey = "CustomFirstOpenKey";

    public AnalyticsModule()
    {

#if !UNITY_EDITOR
        //_services.Add(new AppMetricaProvider());
        //_services.Add(new FacebookProvider());
        //_services.Add(new AppsFlyerProvider());
#endif
        _services.ForEach(x => x.Initialize());        
    }

    public void Start()
    {
        FirstOpenTask().Forget();
        PlayTimeTask().Forget();
    }

    private async UniTaskVoid PlayTimeTask()
    {
        var playTimeMinuteCount = PlayerPrefs.HasKey(_playTimeKey) ? PlayerPrefs.GetInt(_playTimeKey) : 0;

        while (true)
        {
            await UniTask.Delay(TimeSpan.FromSeconds(60), true);

            playTimeMinuteCount++;
            PlayerPrefs.SetInt(_playTimeKey, playTimeMinuteCount);
            SendEvent("PlayTime", ("Minutes", playTimeMinuteCount.ToString()));
        }
    }

    private async UniTaskVoid FirstOpenTask()
    {
        await UniTask.Delay(TimeSpan.FromSeconds(1), true);

        if (!PlayerPrefs.HasKey(_customFirstOpenKey))
        {
            PlayerPrefs.SetInt(_customFirstOpenKey, default);
            SendEvent("StartGame");
        }
    }

    public void SendHeroDieEvent()
    {
        SendEvent("HeroDeath");
    }

    public void SendCoreProgressionUpEvent(string coreProgressionParameterNeme, int stage)
    {
        SendEvent("CoreProgressionUp", (coreProgressionParameterNeme, stage.ToString()));
    }

    public void SendWaveCompleteEvent(int waveNumber)
    {
        SendEvent("WaveComplete", ("WaveNumber", waveNumber.ToString()));
    }

    public void SendBossCompleteEvent(string bossName)
    {
        SendEvent("BossComplete", ("BossName", bossName));
    }

    private void SendEvent(string eventName, params (string, string)[] parameters)
    {
        _services.ForEach(x => x.SendEvent(eventName));
        var sb = new StringBuilder();
        sb.Append($"Send event: \"{eventName}\"");
        if (parameters.Length > 0)
        {
            sb.Append(" with parameters:");
            foreach (var parameter in parameters)
            {
                sb.AppendLine($"\n{parameter.Item1} - \"{parameter.Item2}\"");
            }
        }
        Debug.Log(sb.ToString());
    }
}