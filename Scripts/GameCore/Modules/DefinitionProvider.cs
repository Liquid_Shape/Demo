using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DefinitionsProvider : IDefinitionsProvider
{
    private readonly IReadOnlyList<IDefinition> _allDefinitions;

    private readonly Dictionary<Type, IDefinitionSingle> _singleDefinitionMap = new Dictionary<Type, IDefinitionSingle>();
    private readonly Dictionary<string, IDefinition> _byIdDefinitions = new Dictionary<string, IDefinition>();
    private readonly Dictionary<Type, object> _allByTypes = new Dictionary<Type, object>();

    public DefinitionsProvider(IEnumerable<IDefinition> definitions)
    {
        _allDefinitions = definitions.ToList();
        foreach (var definition in _allDefinitions)
        {
            var type = definition.GetType();
            if (!_allByTypes.TryGetValue(type, out var list))
            {
                list = Activator.CreateInstance(typeof(List<>).MakeGenericType(type));
                _allByTypes.Add(type, list);
            }

            ((IList) list).Add(definition);
            switch (definition)
            {
                case IDefinitionSingle single:
                    _singleDefinitionMap.Add(type, single);
                    break;
                case IDefinitionById byId:
                    _byIdDefinitions.Add(byId.Id, byId);
                    break;
            }
        }
    }

    public T Get<T>() where T : class, IDefinitionSingle => (T) _singleDefinitionMap[typeof(T)];

    public T Get<T>(string id) where T : class, IDefinitionById => (T) _byIdDefinitions[id];

    public bool TryGet<T>(string id, out T result) where T : class, IDefinitionById
    {
        var exist = _byIdDefinitions.TryGetValue(id, out var res);
        result = res as T;
        return exist && result != null;
    }

    public IReadOnlyList<T> GetAllOf<T>() where T : class, IDefinition =>
        _allByTypes[typeof(T)] as IReadOnlyList<T>;

    public IReadOnlyList<IDefinition> GetAll() => _allDefinitions;
}