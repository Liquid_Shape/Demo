using MessagePipe;
using System;
using System.Collections.Generic;
using UnityEngine;
using VContainer.Unity;

public struct CurrencyChangedEvent
{
    public readonly CurrencyType CurrencyType;
    public readonly float NewValue;

    public CurrencyChangedEvent(CurrencyType currencyType, float newValue)
    {
        CurrencyType = currencyType;
        NewValue = newValue;
    }
}

public enum CurrencyType
{
    Exp, Gold
}

public class CurrencyModule : IStartable
{
    private Dictionary<CurrencyType, float> _currencyContainer = new Dictionary<CurrencyType, float>();

    private IPublisher<CurrencyChangedEvent> _currencyChangedEventPublisher;

    public float GetCurrency(CurrencyType currencyType) => _currencyContainer[currencyType];

    public CurrencyModule(IPublisher<CurrencyChangedEvent> currencyChangedEventPublisher)
    {
        _currencyChangedEventPublisher = currencyChangedEventPublisher;
        
        foreach (var currencyType in (CurrencyType[])Enum.GetValues(typeof(CurrencyType)))
        {
            _currencyContainer.Add(currencyType, 0);
        }
    }

    public void AddCurrency(CurrencyType currencyType, float value)
    {
        value = Mathf.Clamp(value, 0f, float.MaxValue);
        _currencyContainer[currencyType] += value;
        SendCurrencyChangedEvent(currencyType);
    }

    public bool TrySpendCurrency(CurrencyType currencyType, float value)
    {
        value = Mathf.Clamp(value, 0f, float.MaxValue);
        if (_currencyContainer[currencyType] < value)
        {
            return false;
        }

        _currencyContainer[currencyType] -= value;
        SendCurrencyChangedEvent(currencyType);

        return true;
    }

    public void Start()
    {
        foreach (var currencyType in (CurrencyType[])Enum.GetValues(typeof(CurrencyType)))
        {
            SendCurrencyChangedEvent(currencyType);
        }
    }

    private void SendCurrencyChangedEvent(CurrencyType currencyType)
    {
        _currencyChangedEventPublisher.Publish(new CurrencyChangedEvent(currencyType, _currencyContainer[currencyType]));
    }
}