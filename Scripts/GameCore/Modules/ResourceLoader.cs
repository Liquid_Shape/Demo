using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine;
using VContainer.Unity;

public interface IResourceLoader
{
    T Load<T>(string name) where T : UnityEngine.Object;
    UniTask<T> LoadAsync<T>(string name) where T : UnityEngine.Object;
}

public class ResourceLoader : IResourceLoader
{
    public const string ResourcesMapName = "ResourcesMap";
    private readonly IReadOnlyDictionary<string, string> _mapping;

    private ResourceLoader(IReadOnlyDictionary<string, string> mapping)
    {
        _mapping = mapping;
    }

    public T Load<T>(string name) where T : UnityEngine.Object
    {
        return Resources.Load<T>(_mapping[name]);
    }

    public async UniTask<T> LoadAsync<T>(string name) where T : UnityEngine.Object
    {
        return await Resources.LoadAsync<T>(_mapping[name]) as T;
    }

    public static ResourceLoader CreateFromGennedResourceMap()
    {
        return new ResourceLoader(JsonConvert.DeserializeObject<Dictionary<string, string>>(
            Resources.Load<TextAsset>(ResourcesMapName).text));
    }
}