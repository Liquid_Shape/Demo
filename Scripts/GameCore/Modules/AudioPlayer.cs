using Cysharp.Threading.Tasks;
using UnityEngine;
using VContainer.Unity;

public enum AudioType
{
    Sound,
    Music
}

public class AudioPlayer : IStartable
{
    private SoundMap _soundMap;
    private AudioData _audioData;
    private Transform _audioContainer;

    private bool _ambientStarted;

    public AudioPlayer(DefinitionsProvider definitionsProvider)
    {
        _soundMap = definitionsProvider.Get<SoundMap>();
        _audioData = definitionsProvider.Get<GlobalData>().AudioData;
    }

    public void Start()
    {
        _audioContainer = new GameObject("AudioContainer").transform;
        GameObject.DontDestroyOnLoad(_audioContainer.gameObject);
    }

    public void PlayAmbient()
    {
        if (_ambientStarted)
        {
            return;
        }

        _ambientStarted = true;
        Play(_audioData.AmbientName, AudioType.Music);
    }

    public void Play(string audioName, AudioType audioType = AudioType.Sound, bool needRandomPitch = false)
    {
        var audioSource = _audioContainer.gameObject.AddComponent<AudioSource>();
        audioSource.clip = _soundMap.GetAudio(audioName);
        audioSource.outputAudioMixerGroup = audioType == AudioType.Sound ?
            _soundMap.SoundAudioMixerGroup : _soundMap.MusicAudioMixerGroup;
        audioSource.loop = audioType == AudioType.Music;
        audioSource.pitch = needRandomPitch ? 1f + Random.Range(-0.2f, 0.2f) : 1f;
        audioSource.Play();
        if (audioType != AudioType.Music)
        {
            RemoveAudioSourceTask().Forget();
        }
        
        async UniTaskVoid RemoveAudioSourceTask()
        {
            await UniTask.Delay(System.TimeSpan.FromSeconds(audioSource.clip.length));
            GameObject.Destroy(audioSource);
        }
    }

    public void PlayUIClickSound()
    {
        Play(_audioData.UIClickSoundName);
    }

    public void PlayUpgradeCoreParameterSound()
    {
        Play(_audioData.UpgradeCoreParameterSoundName);
    }

    public void PlayBossSpawnSound()
    {
        Play(_audioData.BossSpawnSoundName);
    }
}