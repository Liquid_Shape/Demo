using MessagePipe;
using System;
using UnityEngine;
using VContainer;

public struct UIButtonClickEvent
{

}

public class UIManager : MonoBehaviour, IDisposable,
    IMessageHandler<HeroDieEvent>
{
    [SerializeField] private GameOverWindow _gameOverWindow;
    [SerializeField] private UpgradeWindow _upgradeWindow;
    [SerializeField] private WaveCounterPanel _waveCounterPanel;
    [SerializeField] private TimeSpeedPanel _timeSpeedPanel;
    [SerializeField] private OptionPanel _optionPanel;

    private IDisposable _heroDieEventSubscribe;

    [Inject]
    public void Initialize(IObjectResolver objectResolver,
        ISubscriber<HeroDieEvent> heroDieEventSubscriber)
    {
        _heroDieEventSubscribe = heroDieEventSubscriber.Subscribe(this);

        objectResolver.Inject(_gameOverWindow);
        objectResolver.Inject(_upgradeWindow);
        objectResolver.Inject(_waveCounterPanel);        
        objectResolver.Inject(_timeSpeedPanel);
        objectResolver.Inject(_optionPanel);
    }

    public void Handle(HeroDieEvent message)
    {
        _upgradeWindow.gameObject.SetActive(false);
        _timeSpeedPanel.gameObject.SetActive(false);
        _optionPanel.gameObject.SetActive(false);
        _gameOverWindow.gameObject.SetActive(true);
    }

    public void Dispose()
    {
        _upgradeWindow.Dispose();
        _waveCounterPanel.Dispose();
        _heroDieEventSubscribe.Dispose();        
    }
}