using System.Linq;
using UnityEngine;
using VContainer;
using VContainer.Unity;
using MessagePipe;

public class EntryPoint : LifetimeScope
{
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void Initialize()
    {
        DontDestroyOnLoad(new GameObject("EntryPoint", typeof(EntryPoint)));
    }
    
    protected override void Configure(IContainerBuilder builder)
    {
        builder.RegisterInstance(ResourceLoader.CreateFromGennedResourceMap()).AsImplementedInterfaces();
        builder.RegisterInstance(new ProgressModule());
        builder.RegisterEntryPoint<CurrencyModule>().AsSelf();
        builder.RegisterEntryPoint<AudioPlayer>().AsSelf();
        builder.RegisterEntryPoint<AudioEventHandler>().AsSelf();
        builder.Register<AnalyticsModule>(Lifetime.Singleton);

        RegesterDefinitions(builder);
        RegisterEvents(builder);
    }

    private void RegesterDefinitions(IContainerBuilder builder)
    { 
        var definitions = Resources.LoadAll<ScriptableObject>("Configs").OfType<IDefinition>();
        builder.RegisterInstance(new DefinitionsProvider(definitions)).AsImplementedInterfaces();
    }

    private void RegisterEvents(IContainerBuilder builder)
    {
        var options = builder.RegisterMessagePipe();
        options.HandlingSubscribeDisposedPolicy = HandlingSubscribeDisposedPolicy.Ignore;

        builder.RegisterBuildCallback(c => GlobalMessagePipe.SetProvider(c.AsServiceProvider()));
        builder.RegisterMessageBroker<UIButtonClickEvent>(options);
        builder.RegisterMessageBroker<HeroStartDieEvent>(options);
        builder.RegisterMessageBroker<HeroDieEvent>(options);
        builder.RegisterMessageBroker<HeroChangeHealthEvent>(options);
        builder.RegisterMessageBroker<HeroAttackEvent>(options);
        builder.RegisterMessageBroker<HeroTakeHitEvent>(options);
        builder.RegisterMessageBroker<HeroParameterUpdateEvent>(options);
        builder.RegisterMessageBroker<EnemySpawnEvent>(options);
        builder.RegisterMessageBroker<EnemyDieEvent>(options);        
        builder.RegisterMessageBroker<EnemyTakeHitEvent>(options);        
        builder.RegisterMessageBroker<CurrencyChangedEvent>(options);
        builder.RegisterMessageBroker<SpawnWaveStartEvent>(options);
    }
}