using UnityEngine;
using AppsFlyerSDK;
using System.Collections.Generic;

public class AppsFlyerProvider : IAnaliticsServiceProvider
{
    public void Initialize()
    {
        AppsFlyer.initSDK("", null);
        AppsFlyer.startSDK();
    }

    public void SendEvent(string eventName, params (string, string)[] parameters)
    {
        var eventParameters = new Dictionary<string, string>();
        for (int i = 0; i < parameters.Length; i++)
        {
            eventParameters.Add(parameters[i].Item1, parameters[i].Item2);
        }
        AppsFlyer.sendEvent(eventName, eventParameters);
    }
}