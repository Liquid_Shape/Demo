using System.Collections.Generic;

public class AppMetricaProvider : IAnaliticsServiceProvider
{
    private IYandexAppMetrica _metrica;
    
    public void Initialize()
    {
        _metrica = new YandexAppMetricaAndroid();

        var configuration = new YandexAppMetricaConfig("")
        {
            StatisticsSending = true,
            SessionTimeout = 120,
            LocationTracking = false
        };
        _metrica.ActivateWithConfiguration(configuration);
        _metrica.ResumeSession();
        _metrica.SetLocationTracking(false);
    }

    public void SendEvent(string eventName, params (string, string)[] parameters)
    {
        var eventParameters = new Dictionary<string, object>();
        for (int i = 0; i < parameters.Length; i++)
        {
            eventParameters.Add(parameters[i].Item1, parameters[i].Item2);
        }
        _metrica.ReportEvent(eventName, eventParameters);
    }
}