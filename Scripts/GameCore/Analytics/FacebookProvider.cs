using UnityEngine;
using Facebook.Unity;
using System.Collections.Generic;

public class FacebookProvider : IAnaliticsServiceProvider
{
    public void Initialize()
    {        
        if (!FB.IsInitialized)
        {
            FB.Init(() =>
            {
                if (FB.IsInitialized)
                {
                    FB.ActivateApp();
                }
                else
                {
                    Debug.LogError("Failed to Initialize the Facebook SDK");
                }
            });
        }
        else
        {
            FB.ActivateApp();
        }        
    }

    public void SendEvent(string eventName, params (string, string)[] parameters)
    {
        var eventParameters = new Dictionary<string, object>();
        for (int i = 0; i < parameters.Length; i++)
        {
            eventParameters.Add(parameters[i].Item1, parameters[i].Item2);
        }
        FB.LogAppEvent(eventName, null, eventParameters);
    }
}