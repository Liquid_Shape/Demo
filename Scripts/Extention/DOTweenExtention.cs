using DG.Tweening;
using UnityEngine;

public static class DOTweenExtention
{
    public static void DOBubbleAnimation(this Transform target)
    {
        target.DOPunchScale(Vector3.one * 0.35f, 0.5f, 3);
    }
}